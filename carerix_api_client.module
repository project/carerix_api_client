<?php

/**
 * @todo Add file documentation.
 */

/**
 * Implements hook_menu().
 */
function carerix_api_client_menu() {
  $items = array();

  $items['admin/config/carerix/test'] = array(
    'title' => t('Carerix API Client test'),
    'page callback' => 'carerix_api_client_test',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );
  $items['admin/config/carerix'] = array(
    'title' => t('Carerix API Client settings'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('carerix_api_client_settings'),
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * System settings.
 */
function carerix_api_client_settings($form, &$form_state) {
  $form = system_settings_form($form);

  $form['carerix_api_endpoint'] = array(
    '#title' => t('Endpoint URL'),
    '#type' => 'textfield',
    '#default_value' => variable_get('carerix_api_endpoint',
            'https://api.carerix.com'),
  );
  $form['carerix_api_system'] = array(
    '#title' => t('System'),
    '#type' => 'textfield',
    '#default_value' => variable_get('carerix_api_system', 'publictest'),
  );
  $form['carerix_api_token'] = array(
    '#title' => t('Token'),
    '#type' => 'textfield',
    '#default_value' => variable_get('carerix_api_token',
            '480150ff6a3751875400587b21f85bae7576f2ea5b6ca05e'),
  );
  $form['carerix_api_proxy'] = array(
    '#title' => t('Proxy URL'),
    '#description' => t('Set proxy (optional, can be used for HTTP traffic
      debugging or if your service is behind a firewall). E.g.
      \'localhost:8888\'. If you don\'t know what it is just ignore it.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('carerix_api_proxy', ''),
  );

  return $form;
}

/**
 * Implements hook_libraries_info().
 */
function carerix_api_client_libraries_info() {
  $libraries = array();

  $libraries['carerix_api_client'] = array(
    'name' => 'Carerix PHP API Client',
    'vendor url' => 'http://development.wiki.carerix.com/cxwiki/doku.php?id=cxrest_api_client#getting_installing',
    'download url' => 'http://pear.carerix.com/',
    'version arguments' => array(
      'file' => 'package.xml',
      'pattern' => '/<api>([0-9a-zA-Z\.-]+)<\/api>/',
      'lines' => 23,
    ),
    'files' => array(
      'php' => array('Carerix_Api_Rest-@version/Carerix/Api/Rest/Loader.php'),
    ),
  );

  return $libraries;
}

/**
 * Load the Carerix API Client.
 */
function carerix_api_client_load() {
  // Try to load the library and check if that worked.
  if (($library = libraries_load('carerix_api_client')) && !empty($library['loaded'])) {
    // Set include path.
    $path = DRUPAL_ROOT . DIRECTORY_SEPARATOR . $library['library path']
            . DIRECTORY_SEPARATOR . 'Carerix_Api_Rest-' . $library['version'];
    set_include_path(get_include_path() . PATH_SEPARATOR . $path);
    // Autoload classes. This way we don't have to call require_once for each
    // class.
    spl_autoload_register(array('Carerix_Api_Rest_Loader', 'autoload'));

    // Initialize client.
    $client = new Carerix_Api_Rest_Client();
    
    // Initialize client manager.
    $manager = new Carerix_Api_Rest_Manager($client);

    // Auto-discover all built-in entities.
    $manager->autoDiscoverEntities();

    // Set API endpoint.
    $endpoint = variable_get('carerix_api_endpoint', 'https://api.carerix.com');
    $manager->setUrl($endpoint);

    // Set system name.
    $system = variable_get('carerix_api_system', 'publictest');
    $manager->setUsername($system);

    // Set user token.
    $token = variable_get('carerix_api_token', '480150ff6a3751875400587b21f85bae7576f2ea5b6ca05e');
    $manager->setPassword($token);

    // Set proxy (optional, can be used for HTTP traffic debugging or if your
    // service is behind a firewall)
    // If you don't know what it is just ignore it.
    $proxy = variable_get('carerix_api_proxy', '');
    if ($proxy) {
      $manager->setProxy($proxy);
    }

    // register entity manager
    Carerix_Api_Rest_Entity::setManager($manager);

    return $client;
  }

  return FALSE;
}

/**
 * Test the Carerix API Client.
 */
function carerix_api_client_test() {
  // Load the client.
  $client = carerix_api_client_load();
  if (is_object($client)) {
    return 'Loaded!';
  }

  return 'Not loaded!';
}
